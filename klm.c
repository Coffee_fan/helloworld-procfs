#include <linux/init.h>
#include <linux/module.h>
// #include <linux/kernel.h>
MODULE_LICENSE("GPL");

static int __init klm_init(void) {
	printk(KERN_INFO "Passing through %s\n", __FUNCTION__);
	return 0;
}

static void __exit klm_exit(void) {
	printk(KERN_INFO "Passing through %s\n", __FUNCTION__);
}

module_init(klm_init);
module_exit(klm_exit);


